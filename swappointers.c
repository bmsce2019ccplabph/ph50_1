#include <stdio.h>
void swap(int *,int *);
int main()
{
    int x,y;
    printf("enter first number,x:\n");
    scanf("%d",&x);
    printf("enter second number,y:\n");
    scanf("%d",&y);
    swap(&x,&y);
    printf("after swapping x=%d and y=%d\n",x,y);
    return 0;
}
void swap(int *m,int *n)
{
    int temp;
    temp=m;
    *m=*n;
    *n=temp;
    printf("in function m=%d and n=%d\n",*m,*n);
}

